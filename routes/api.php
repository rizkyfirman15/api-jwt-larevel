<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::middleware('auth:api')->group(function () {
    Route::post('catalogue/new', 'KatalogBukuController@store');
    Route::patch('catalogue/{katalog_buku}/edited', 'KatalogBukuController@update');
    Route::delete('catalogue/{katalog_buku}/delete', 'KatalogBukuController@destroy');

    Route::post('borrow', 'PeminjamanController@store');
    Route::patch('return', 'PeminjamanController@update');

    Route::post('profile/', 'MahasiswaController@store');
    Route::patch('profile/edited', 'MahasiswaController@update');
});

Route::get('catalogue', 'KatalogBukuController@index');
Route::get('catalogue/{katalog_buku}', 'KatalogBukuController@show');

Route::get('user', 'UserController');
