<?php

namespace App\Http\Controllers;

use App\Http\Resources\MahasiswaResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Mahasiswa;


class MahasiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'username' => ['required'],
            'nama' => [],
            'nim' => [],
            'fakultas' => [],
            'jurusan' => [],
            'no_hp' => [],
            'no_wa' => [],
        ]);

        $user_id = User::where('username', request('username'))->value('id');
        $user_role = User::where('username', request('username'))->value('role_id');
        if ($user_id == null || $user_role != 0) {
            return response()->json('Username tidak terdaftar!', 404);
        }

        $profile = Mahasiswa::create([
            'nama' => request('nama'),
            'nim' => request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'no_hp' => request('no_hp'),
            'no_wa' => request('no_wa'),
            'user_id' => $user_id
        ]);

        return $profile;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Mahasiswa $mahasiswa)
    {
        // return new MahasiswaResource($mahasiswa);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $request->validate([
            'username' => ['required'],
            'nama' => [],
            'nim' => [],
            'fakultas' => [],
            'jurusan' => [],
            'no_hp' => [],
            'no_wa' => [],
        ]);

        $user_id = User::where('username', request('username'))->value('id');
        $user_role = User::where('username', request('username'))->value('role_id');
        if ($user_id == null || $user_role != 0) {
            return response()->json('Username tidak terdaftar!', 404);
        }

        $profile = Mahasiswa::where('user_id', $user_id)->update([
            'nama' => request('nama'),
            'nim' => request('nim'),
            'fakultas' => request('fakultas'),
            'jurusan' => request('jurusan'),
            'no_hp' => request('no_hp'),
            'no_wa' => request('no_wa'),
        ]);

        if ($profile) {
            return response()->json('Profile telah diperbaharui!', 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function mahasiswaStore()
    {
    }
}
