<?php

namespace App\Http\Controllers;

use App\Models\Peminjaman;
use App\Models\KatalogBuku;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Str;

class PeminjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (auth()->user()->isRole() === 'Admin') {
            $request->validate([
                'kode_buku' => ['required'],
                'username' => ['required'],
            ]);

            $katalog_buku_id = KatalogBuku::where('kode', request('kode_buku'))->value('id');
            $user_id = User::where('username', request('username'))->value('id');
        } else {
            $request->validate([
                'kode_buku' => ['required'],
            ]);

            $katalog_buku_id = KatalogBuku::where('kode', request('kode_buku'))->value('id');
            $user_id = Auth::id();
        }

        do {
            $token = Str::random(12);
        } while (Peminjaman::where("token_peminjaman", $token)->first() != null);

        $waktu_peminjaman = Carbon::now();
        $batas_pengembalian = $waktu_peminjaman->addDays(7);
        $batas_pengembalian->hour = 23;
        $batas_pengembalian->minute = 59;
        $batas_pengembalian->second = 59;

        $pinjam = Peminjaman::create([
            'katalog_buku_id' => $katalog_buku_id,
            'user_id' => $user_id,
            'waktu_peminjaman' => $waktu_peminjaman,
            'batas_pengembalian' => $batas_pengembalian,
            'token_peminjaman' => $token
        ]);

        return response()->json('Token peminjaman: ' . $token, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (auth()->user()->isRole() !== 'Admin') {
            return response(null, 401);
        }
        $request->validate([
            'token_peminjaman' => ['required'],
        ]);

        if (Peminjaman::where("token_peminjaman", request('token_peminjaman'))->first() != null) {
            $waktu_pengembalian = Carbon::now();
            $cek1 = $waktu_pengembalian->toDateTimeString();
            $batas_pengembalian = Peminjaman::where("token_peminjaman", request('token_peminjaman'))->value('batas_pengembalian');

            $status_ontime = false;
            $str_ontime = "";

            if (Carbon::parse($cek1)->lt(Carbon::parse($batas_pengembalian))) {
                $status_ontime = true;
                $str_ontime = "dengan tepat waktu!";
            }

            $kembali = Peminjaman::where("token_peminjaman", request('token_peminjaman'))->update([
                'status_pengembalian' => true,
                'waktu_pengembalian' => $waktu_pengembalian,
                'status_ontime' => $status_ontime
            ]);

            if ($kembali) {
                return response()->json('Buku telah dikembalikan ' . $str_ontime, 200);
            }
        } else {
            return response()->json('Token tidak terdaftar!', 404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
