<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KatalogBuku;
use App\Http\Resources\KatalogBukuResource;
use App\Http\Requests\KatalogBukuRequest;


class KatalogBukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $katalog_buku = KatalogBuku::get();
        return KatalogBukuResource::collection($katalog_buku);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(KatalogBukuRequest $request)
    {
        if (auth()->user()->isRole() === 'Admin') {
            $buku = KatalogBuku::create($this->katalogStore());
            return $buku;
        } else {
            return response(null, 401);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(KatalogBuku $katalog_buku)
    {
        return new KatalogBukuResource($katalog_buku);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(KatalogBukuRequest $request, KatalogBuku $katalog_buku)
    {
        if (auth()->user()->isRole() === 'Admin') {
            $buku = KatalogBuku::where('kode', $katalog_buku->kode)->update($this->katalogStore());
            return $buku;
        } else {
            return response(null, 401);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(KatalogBuku $katalog_buku)
    {
        $kode = $katalog_buku->kode;
        if (auth()->user()->isRole() === 'Admin') {
            $katalog_buku->delete();
            return response()->json('Buku seri ' . $kode . ' telah dihapus!', 200);
        } else {
            return response(null, 401);
        }
    }

    public function katalogStore()
    {
        return [
            'kode' => request('kode'),
            'judul' => request('judul'),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => request('tahun_terbit')
        ];
    }
}
