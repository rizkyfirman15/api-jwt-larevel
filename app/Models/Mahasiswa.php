<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Mahasiswa extends Model
{
    protected $table = 'mahasiswa';
    protected $fillable = [
        'nama',
        'nim',
        'fakultas',
        'jurusan',
        'no_hp',
        'no_wa',
        'user_id'
    ];

    public function getRouteKeyName()
    {
        return 'nim';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
