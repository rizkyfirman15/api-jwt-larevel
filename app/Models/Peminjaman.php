<?php

namespace App\Models;

use App\Models\User;
use App\Models\KatalogBuku;
use Illuminate\Database\Eloquent\Model;

class Peminjaman extends Model
{
    protected $table = 'peminjaman';
    protected $fillable = [
        'katalog_buku_id',
        'user_id',
        'waktu_peminjaman',
        'batas_pengembalian',
        'status_pengembalian',
        'waktu_pengembalian',
        'status_ontime',
        'token_peminjaman'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function katalog_buku()
    {
        return $this->belongsTo(KatalogBuku::class);
    }
}
