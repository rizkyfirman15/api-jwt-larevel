<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Peminjaman;

class KatalogBuku extends Model
{
    protected $table = 'katalog_buku';
    protected $fillable = [
        'kode',
        'judul',
        'pengarang',
        'tahun_terbit',
    ];

    public function getRouteKeyName()
    {
        return 'kode';
    }

    public function peminjaman()
    {
        return $this->hasMany(Peminjaman::class);
    }
}
